﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;
using System.Collections.Specialized;
using System.Runtime;
using System.Collections;
using System.Net;

namespace HS_CSharpUtility.Web
{
    public sealed class HttpUtility
    {
        // Methods
        internal static string AspCompatUrlEncode(string s)
        {
            s = UrlEncode(s);
            s = s.Replace("!", "%21");
            s = s.Replace("*", "%2A");
            s = s.Replace("(", "%28");
            s = s.Replace(")", "%29");
            s = s.Replace("-", "%2D");
            s = s.Replace(".", "%2E");
            s = s.Replace("_", "%5F");
            s = s.Replace(@"\", "%5C");
            return s;
        }

        internal static string FormatHttpCookieDateTime(DateTime dt)
        {
            if ((dt < DateTime.MaxValue.AddDays(-1.0)) && (dt > DateTime.MinValue.AddDays(1.0)))
            {
                dt = dt.ToUniversalTime();
            }
            return dt.ToString("ddd, dd-MMM-yyyy HH':'mm':'ss 'GMT'", DateTimeFormatInfo.InvariantInfo);
        }

        internal static string FormatHttpDateTime(DateTime dt)
        {
            if ((dt < DateTime.MaxValue.AddDays(-1.0)) && (dt > DateTime.MinValue.AddDays(1.0)))
            {
                dt = dt.ToUniversalTime();
            }
            return dt.ToString("R", DateTimeFormatInfo.InvariantInfo);
        }

        internal static string FormatHttpDateTimeUtc(DateTime dt)
        {
            return dt.ToString("R", DateTimeFormatInfo.InvariantInfo);
        }

        internal static string FormatPlainTextAsHtml(string s)
        {
            if (s == null)
            {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            StringWriter output = new StringWriter(sb);
            FormatPlainTextAsHtml(s, output);
            return sb.ToString();
        }

        internal static void FormatPlainTextAsHtml(string s, TextWriter output)
        {
            if (s != null)
            {
                int length = s.Length;
                char ch = '\0';
                for (int i = 0; i < length; i++)
                {
                    char ch2 = s[i];
                    switch (ch2)
                    {
                        case '\n':
                            output.Write("<br>");
                            goto Label_0113;

                        case '\r':
                            goto Label_0113;

                        case ' ':
                            if (ch != ' ')
                            {
                                break;
                            }
                            output.Write("&nbsp;");
                            goto Label_0113;

                        case '"':
                            output.Write("&quot;");
                            goto Label_0113;

                        case '&':
                            output.Write("&amp;");
                            goto Label_0113;

                        case '<':
                            output.Write("&lt;");
                            goto Label_0113;

                        case '>':
                            output.Write("&gt;");
                            goto Label_0113;

                        default:
                            if ((ch2 >= '\x00a0') && (ch2 < 'Ā'))
                            {
                                output.Write("&#");
                                output.Write(((int)ch2).ToString(NumberFormatInfo.InvariantInfo));
                                output.Write(';');
                            }
                            else
                            {
                                output.Write(ch2);
                            }
                            goto Label_0113;
                    }
                    output.Write(ch2);
                Label_0113:
                    ch = ch2;
                }
            }
        }

        internal static string FormatPlainTextSpacesAsHtml(string s)
        {
            if (s == null)
            {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            StringWriter writer = new StringWriter(sb);
            int length = s.Length;
            for (int i = 0; i < length; i++)
            {
                char ch = s[i];
                if (ch == ' ')
                {
                    writer.Write("&nbsp;");
                }
                else
                {
                    writer.Write(ch);
                }
            }
            return sb.ToString();
        }

        public static string HtmlAttributeEncode(string s)
        {
            return HttpEncoder.Current.HtmlAttributeEncode(s);
        }

        public static void HtmlAttributeEncode(string s, TextWriter output)
        {
            HttpEncoder.Current.HtmlAttributeEncode(s, output);
        }

        public static string HtmlDecode(string s)
        {
            return HttpEncoder.Current.HtmlDecode(s);
        }

        public static void HtmlDecode(string s, TextWriter output)
        {
            HttpEncoder.Current.HtmlDecode(s, output);
        }

        public static string HtmlEncode(object value)
        {
            if (value == null)
            {
                return null;
            }
            IHtmlString str = value as IHtmlString;
            if (str != null)
            {
                return str.ToHtmlString();
            }
            return HtmlEncode(Convert.ToString(value, CultureInfo.CurrentCulture));
        }

        public static string HtmlEncode(string s)
        {
            return HttpEncoder.Current.HtmlEncode(s);
        }

        public static void HtmlEncode(string s, TextWriter output)
        {
            HttpEncoder.Current.HtmlEncode(s, output);
        }

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public static string JavaScriptStringEncode(string value)
        {
            return JavaScriptStringEncode(value, false);
        }

        public static string JavaScriptStringEncode(string value, bool addDoubleQuotes)
        {
            string str = HttpEncoder.Current.JavaScriptStringEncode(value);
            if (!addDoubleQuotes)
            {
                return str;
            }
            return ("\"" + str + "\"");
        }

        public static NameValueCollection ParseQueryString(string query)
        {
            return ParseQueryString(query, Encoding.UTF8);
        }

        public static NameValueCollection ParseQueryString(string query, Encoding encoding)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            if ((query.Length > 0) && (query[0] == '?'))
            {
                query = query.Substring(1);
            }
            return new HttpValueCollection(query, false, true, encoding);
        }

        public static string UrlDecode(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlDecode(str, Encoding.UTF8);
        }

        public static string UrlDecode(byte[] bytes, Encoding e)
        {
            if (bytes == null)
            {
                return null;
            }
            return UrlDecode(bytes, 0, bytes.Length, e);
        }

        public static string UrlDecode(string str, Encoding e)
        {
            return HttpEncoder.Current.UrlDecode(str, e);
        }

        public static string UrlDecode(byte[] bytes, int offset, int count, Encoding e)
        {
            return HttpEncoder.Current.UrlDecode(bytes, offset, count, e);
        }

        public static byte[] UrlDecodeToBytes(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            return UrlDecodeToBytes(bytes, 0, (bytes != null) ? bytes.Length : 0);
        }

        public static byte[] UrlDecodeToBytes(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlDecodeToBytes(str, Encoding.UTF8);
        }

        public static byte[] UrlDecodeToBytes(string str, Encoding e)
        {
            if (str == null)
            {
                return null;
            }
            return UrlDecodeToBytes(e.GetBytes(str));
        }

        public static byte[] UrlDecodeToBytes(byte[] bytes, int offset, int count)
        {
            return HttpEncoder.Current.UrlDecode(bytes, offset, count);
        }

        public static string UrlEncode(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlEncode(str, Encoding.UTF8);
        }

        public static string UrlEncode(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(UrlEncodeToBytes(bytes));
        }

        public static string UrlEncode(string str, Encoding e)
        {
            if (str == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(UrlEncodeToBytes(str, e));
        }

        public static string UrlEncode(byte[] bytes, int offset, int count)
        {
            if (bytes == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(UrlEncodeToBytes(bytes, offset, count));
        }

        internal static string UrlEncodeNonAscii(string str, Encoding e)
        {
            return HttpEncoder.Current.UrlEncodeNonAscii(str, e);
        }

        public static byte[] UrlEncodeToBytes(byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            return UrlEncodeToBytes(bytes, 0, bytes.Length);
        }

        public static byte[] UrlEncodeToBytes(string str)
        {
            if (str == null)
            {
                return null;
            }
            return UrlEncodeToBytes(str, Encoding.UTF8);
        }

        public static byte[] UrlEncodeToBytes(string str, Encoding e)
        {
            if (str == null)
            {
                return null;
            }
            byte[] bytes = e.GetBytes(str);
            return HttpEncoder.Current.UrlEncode(bytes, 0, bytes.Length, false);
        }

        public static byte[] UrlEncodeToBytes(byte[] bytes, int offset, int count)
        {
            return HttpEncoder.Current.UrlEncode(bytes, offset, count, true);
        }

        public static string UrlEncodeUnicode(string str)
        {
            return HttpEncoder.Current.UrlEncodeUnicode(str, false);
        }

        public static byte[] UrlEncodeUnicodeToBytes(string str)
        {
            if (str == null)
            {
                return null;
            }
            return Encoding.ASCII.GetBytes(UrlEncodeUnicode(str));
        }

        public static string UrlPathEncode(string str)
        {
            return HttpEncoder.Current.UrlPathEncode(str);
        }
    }
    public class HttpEncoder
{
    // Fields
    private static HttpEncoder _customEncoder;
    private static readonly Lazy<HttpEncoder> _customEncoderResolver = new Lazy<HttpEncoder>(new Func<HttpEncoder>(HttpEncoder.GetCustomEncoderFromConfig));
    private static readonly HttpEncoder _defaultEncoder = new HttpEncoder();
    private static readonly string[] _headerEncodingTable = new string[] { 
        "%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07", "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f", 
        "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17", "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f"
     };
    private static bool _isDefaultEncoder = true;

    // Methods
    private static void AppendCharAsUnicodeJavaScript(StringBuilder builder, char c)
    {
        builder.Append(@"\u");
        builder.Append(((int) c).ToString("x4", CultureInfo.InvariantCulture));
    }

    internal static string CollapsePercentUFromStringInternal(string s, Encoding e)
    {
        int length = s.Length;
        UrlDecoder decoder = new UrlDecoder(length, e);
        if (s.IndexOf("%u", StringComparison.Ordinal) == -1)
        {
            return s;
        }
        for (int i = 0; i < length; i++)
        {
            char ch = s[i];
            if (((ch == '%') && (i < (length - 5))) && (s[i + 1] == 'u'))
            {
                int num4 = HttpEncoderUtility.HexToInt(s[i + 2]);
                int num5 = HttpEncoderUtility.HexToInt(s[i + 3]);
                int num6 = HttpEncoderUtility.HexToInt(s[i + 4]);
                int num7 = HttpEncoderUtility.HexToInt(s[i + 5]);
                if (((num4 >= 0) && (num5 >= 0)) && ((num6 >= 0) && (num7 >= 0)))
                {
                    ch = (char) ((((num4 << 12) | (num5 << 8)) | (num6 << 4)) | num7);
                    i += 5;
                    decoder.AddChar(ch);
                    continue;
                }
            }
            if ((ch & 0xff80) == 0)
            {
                decoder.AddByte((byte) ch);
            }
            else
            {
                decoder.AddChar(ch);
            }
        }
        return decoder.GetString();
    }

    private static HttpEncoder GetCustomEncoderFromConfig()
    {
        HttpRuntimeSection httpRuntime = RuntimeConfig.GetAppConfig().HttpRuntime;
        Type userBaseType = ConfigUtil.GetType(httpRuntime.EncoderType, "encoderType", httpRuntime);
        _isDefaultEncoder = userBaseType == typeof(HttpEncoder);
        ConfigUtil.CheckBaseType(typeof(HttpEncoder), userBaseType, "encoderType", httpRuntime);
        return (HttpEncoder) HttpRuntime.CreatePublicInstance(userBaseType);
    }

    private static string HeaderEncodeInternal(string value)
    {
        string str = value;
        if (!HeaderValueNeedsEncoding(value))
        {
            return str;
        }
        StringBuilder builder = new StringBuilder();
        foreach (char ch in value)
        {
            if ((ch < ' ') && (ch != '\t'))
            {
                builder.Append(_headerEncodingTable[ch]);
            }
            else if (ch == '\x007f')
            {
                builder.Append("%7f");
            }
            else
            {
                builder.Append(ch);
            }
        }
        return builder.ToString();
    }

    protected internal virtual void HeaderNameValueEncode(string headerName, string headerValue, out string encodedHeaderName, out string encodedHeaderValue)
    {
        encodedHeaderName = string.IsNullOrEmpty(headerName) ? headerName : HeaderEncodeInternal(headerName);
        encodedHeaderValue = string.IsNullOrEmpty(headerValue) ? headerValue : HeaderEncodeInternal(headerValue);
    }

    private static bool HeaderValueNeedsEncoding(string value)
    {
        foreach (char ch in value)
        {
            if (((ch < ' ') && (ch != '\t')) || (ch == '\x007f'))
            {
                return true;
            }
        }
        return false;
    }

    internal string HtmlAttributeEncode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return value;
        }
        if (_isDefaultEncoder && (IndexOfHtmlAttributeEncodingChars(value, 0) == -1))
        {
            return value;
        }
        StringWriter output = new StringWriter(CultureInfo.InvariantCulture);
        this.HtmlAttributeEncode(value, output);
        return output.ToString();
    }

    protected internal virtual void HtmlAttributeEncode(string value, TextWriter output)
    {
        if (value != null)
        {
            if (output == null)
            {
                throw new ArgumentNullException("output");
            }
            HttpWriter writer = output as HttpWriter;
            if (writer != null)
            {
                HtmlAttributeEncodeInternal(value, writer);
            }
            else
            {
                HtmlAttributeEncodeInternal(value, output);
            }
        }
    }

    private static unsafe void HtmlAttributeEncodeInternal(string s, TextWriter output)
    {
        int num = IndexOfHtmlAttributeEncodingChars(s, 0);
        if (num == -1)
        {
            output.Write(s);
        }
        else
        {
            int num2 = s.Length - num;
            fixed (char* str = ((char*) s))
            {
                char* chPtr = str;
                char* chPtr2 = chPtr;
                while (num-- > 0)
                {
                    chPtr2++;
                    output.Write(chPtr2[0]);
                }
                while (num2-- > 0)
                {
                    chPtr2++;
                    char ch = chPtr2[0];
                    if (ch <= '<')
                    {
                        switch (ch)
                        {
                            case '&':
                            {
                                output.Write("&amp;");
                                continue;
                            }
                            case '\'':
                            {
                                output.Write("&#39;");
                                continue;
                            }
                            case '"':
                            {
                                output.Write("&quot;");
                                continue;
                            }
                            case '<':
                            {
                                output.Write("&lt;");
                                continue;
                            }
                        }
                        output.Write(ch);
                    }
                    else
                    {
                        output.Write(ch);
                    }
                }
            }
        }
    }

    private static void HtmlAttributeEncodeInternal(string value, HttpWriter writer)
    {
        int num = IndexOfHtmlAttributeEncodingChars(value, 0);
        if (num == -1)
        {
            writer.Write(value);
            return;
        }
        int length = value.Length;
        int index = 0;
    Label_001D:
        if (num > index)
        {
            writer.WriteString(value, index, num - index);
        }
        switch (value[num])
        {
            case '&':
                writer.Write("&amp;");
                break;

            case '\'':
                writer.Write("&#39;");
                break;

            case '<':
                writer.Write("&lt;");
                break;

            case '"':
                writer.Write("&quot;");
                break;
        }
        index = num + 1;
        if (index < length)
        {
            num = IndexOfHtmlAttributeEncodingChars(value, index);
            if (num != -1)
            {
                goto Label_001D;
            }
            writer.WriteString(value, index, length - index);
        }
    }

    internal string HtmlDecode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return value;
        }
        if (_isDefaultEncoder)
        {
            return WebUtility.HtmlDecode(value);
        }
        StringWriter output = new StringWriter(CultureInfo.InvariantCulture);
        this.HtmlDecode(value, output);
        return output.ToString();
    }

    protected internal virtual void HtmlDecode(string value, TextWriter output)
    {
        WebUtility.HtmlDecode(value, output);
    }

    internal string HtmlEncode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return value;
        }
        if (_isDefaultEncoder)
        {
            return WebUtility.HtmlEncode(value);
        }
        StringWriter output = new StringWriter(CultureInfo.InvariantCulture);
        this.HtmlEncode(value, output);
        return output.ToString();
    }

    protected internal virtual void HtmlEncode(string value, TextWriter output)
    {
        WebUtility.HtmlEncode(value, output);
    }

    private static unsafe int IndexOfHtmlAttributeEncodingChars(string s, int startPos)
    {
        int num = s.Length - startPos;
        fixed (char* str = ((char*) s))
        {
            char* chPtr = str;
            char* chPtr2 = chPtr + startPos;
            while (num > 0)
            {
                char ch = chPtr2[0];
                if (ch <= '<')
                {
                    switch (ch)
                    {
                        case '&':
                        case '\'':
                        case '<':
                        case '"':
                            return (s.Length - num);
                    }
                }
                chPtr2++;
                num--;
            }
        }
        return -1;
    }

    internal static void InitializeOnFirstRequest()
    {
        HttpEncoder local1 = _customEncoderResolver.Value;
    }

    private static bool IsNonAsciiByte(byte b)
    {
        if (b < 0x7f)
        {
            return (b < 0x20);
        }
        return true;
    }

    internal string JavaScriptStringEncode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }
        StringBuilder builder = null;
        int startIndex = 0;
        int count = 0;
        for (int i = 0; i < value.Length; i++)
        {
            char c = value[i];
            if ((((c == '\r') || (c == '\t')) || ((c == '"') || (c == '\''))) || ((((c == '<') || (c == '>')) || ((c == '\\') || (c == '\n'))) || (((c == '\b') || (c == '\f')) || (c < ' '))))
            {
                if (builder == null)
                {
                    builder = new StringBuilder(value.Length + 5);
                }
                if (count > 0)
                {
                    builder.Append(value, startIndex, count);
                }
                startIndex = i + 1;
                count = 0;
            }
            switch (c)
            {
                case '<':
                case '>':
                case '\'':
                {
                    AppendCharAsUnicodeJavaScript(builder, c);
                    continue;
                }
                case '\\':
                {
                    builder.Append(@"\\");
                    continue;
                }
                case '\b':
                {
                    builder.Append(@"\b");
                    continue;
                }
                case '\t':
                {
                    builder.Append(@"\t");
                    continue;
                }
                case '\n':
                {
                    builder.Append(@"\n");
                    continue;
                }
                case '\f':
                {
                    builder.Append(@"\f");
                    continue;
                }
                case '\r':
                {
                    builder.Append(@"\r");
                    continue;
                }
                case '"':
                {
                    builder.Append("\\\"");
                    continue;
                }
            }
            if (c < ' ')
            {
                AppendCharAsUnicodeJavaScript(builder, c);
            }
            else
            {
                count++;
            }
        }
        if (builder == null)
        {
            return value;
        }
        if (count > 0)
        {
            builder.Append(value, startIndex, count);
        }
        return builder.ToString();
    }

    internal string UrlDecode(string value, Encoding encoding)
    {
        if (value == null)
        {
            return null;
        }
        int length = value.Length;
        UrlDecoder decoder = new UrlDecoder(length, encoding);
        for (int i = 0; i < length; i++)
        {
            char ch = value[i];
            if (ch == '+')
            {
                ch = ' ';
            }
            else if ((ch == '%') && (i < (length - 2)))
            {
                if ((value[i + 1] == 'u') && (i < (length - 5)))
                {
                    int num3 = HttpEncoderUtility.HexToInt(value[i + 2]);
                    int num4 = HttpEncoderUtility.HexToInt(value[i + 3]);
                    int num5 = HttpEncoderUtility.HexToInt(value[i + 4]);
                    int num6 = HttpEncoderUtility.HexToInt(value[i + 5]);
                    if (((num3 < 0) || (num4 < 0)) || ((num5 < 0) || (num6 < 0)))
                    {
                        goto Label_010B;
                    }
                    ch = (char) ((((num3 << 12) | (num4 << 8)) | (num5 << 4)) | num6);
                    i += 5;
                    decoder.AddChar(ch);
                    continue;
                }
                int num7 = HttpEncoderUtility.HexToInt(value[i + 1]);
                int num8 = HttpEncoderUtility.HexToInt(value[i + 2]);
                if ((num7 >= 0) && (num8 >= 0))
                {
                    byte b = (byte) ((num7 << 4) | num8);
                    i += 2;
                    decoder.AddByte(b);
                    continue;
                }
            }
        Label_010B:
            if ((ch & 0xff80) == 0)
            {
                decoder.AddByte((byte) ch);
            }
            else
            {
                decoder.AddChar(ch);
            }
        }
        return decoder.GetString();
    }

    internal byte[] UrlDecode(byte[] bytes, int offset, int count)
    {
        if (!ValidateUrlEncodingParameters(bytes, offset, count))
        {
            return null;
        }
        int length = 0;
        byte[] sourceArray = new byte[count];
        for (int i = 0; i < count; i++)
        {
            int index = offset + i;
            byte num4 = bytes[index];
            if (num4 == 0x2b)
            {
                num4 = 0x20;
            }
            else if ((num4 == 0x25) && (i < (count - 2)))
            {
                int num5 = HttpEncoderUtility.HexToInt((char) bytes[index + 1]);
                int num6 = HttpEncoderUtility.HexToInt((char) bytes[index + 2]);
                if ((num5 >= 0) && (num6 >= 0))
                {
                    num4 = (byte) ((num5 << 4) | num6);
                    i += 2;
                }
            }
            sourceArray[length++] = num4;
        }
        if (length < sourceArray.Length)
        {
            byte[] destinationArray = new byte[length];
            Array.Copy(sourceArray, destinationArray, length);
            sourceArray = destinationArray;
        }
        return sourceArray;
    }

    internal string UrlDecode(byte[] bytes, int offset, int count, Encoding encoding)
    {
        if (!ValidateUrlEncodingParameters(bytes, offset, count))
        {
            return null;
        }
        UrlDecoder decoder = new UrlDecoder(count, encoding);
        for (int i = 0; i < count; i++)
        {
            int index = offset + i;
            byte b = bytes[index];
            if (b == 0x2b)
            {
                b = 0x20;
            }
            else if ((b == 0x25) && (i < (count - 2)))
            {
                if ((bytes[index + 1] == 0x75) && (i < (count - 5)))
                {
                    int num4 = HttpEncoderUtility.HexToInt((char) bytes[index + 2]);
                    int num5 = HttpEncoderUtility.HexToInt((char) bytes[index + 3]);
                    int num6 = HttpEncoderUtility.HexToInt((char) bytes[index + 4]);
                    int num7 = HttpEncoderUtility.HexToInt((char) bytes[index + 5]);
                    if (((num4 < 0) || (num5 < 0)) || ((num6 < 0) || (num7 < 0)))
                    {
                        goto Label_00E7;
                    }
                    char ch = (char) ((((num4 << 12) | (num5 << 8)) | (num6 << 4)) | num7);
                    i += 5;
                    decoder.AddChar(ch);
                    continue;
                }
                int num8 = HttpEncoderUtility.HexToInt((char) bytes[index + 1]);
                int num9 = HttpEncoderUtility.HexToInt((char) bytes[index + 2]);
                if ((num8 >= 0) && (num9 >= 0))
                {
                    b = (byte) ((num8 << 4) | num9);
                    i += 2;
                }
            }
        Label_00E7:
            decoder.AddByte(b);
        }
        return decoder.GetString();
    }

    protected internal virtual byte[] UrlEncode(byte[] bytes, int offset, int count)
    {
        if (!ValidateUrlEncodingParameters(bytes, offset, count))
        {
            return null;
        }
        int num = 0;
        int num2 = 0;
        for (int i = 0; i < count; i++)
        {
            char ch = (char) bytes[offset + i];
            if (ch == ' ')
            {
                num++;
            }
            else if (!HttpEncoderUtility.IsUrlSafeChar(ch))
            {
                num2++;
            }
        }
        if ((num == 0) && (num2 == 0))
        {
            return bytes;
        }
        byte[] buffer = new byte[count + (num2 * 2)];
        int num4 = 0;
        for (int j = 0; j < count; j++)
        {
            byte num6 = bytes[offset + j];
            char ch2 = (char) num6;
            if (HttpEncoderUtility.IsUrlSafeChar(ch2))
            {
                buffer[num4++] = num6;
            }
            else if (ch2 == ' ')
            {
                buffer[num4++] = 0x2b;
            }
            else
            {
                buffer[num4++] = 0x25;
                buffer[num4++] = (byte) HttpEncoderUtility.IntToHex((num6 >> 4) & 15);
                buffer[num4++] = (byte) HttpEncoderUtility.IntToHex(num6 & 15);
            }
        }
        return buffer;
    }

    internal byte[] UrlEncode(byte[] bytes, int offset, int count, bool alwaysCreateNewReturnValue)
    {
        byte[] buffer = this.UrlEncode(bytes, offset, count);
        if ((alwaysCreateNewReturnValue && (buffer != null)) && (buffer == bytes))
        {
            return (byte[]) buffer.Clone();
        }
        return buffer;
    }

    internal string UrlEncodeNonAscii(string str, Encoding e)
    {
        if (string.IsNullOrEmpty(str))
        {
            return str;
        }
        if (e == null)
        {
            e = Encoding.UTF8;
        }
        byte[] bytes = e.GetBytes(str);
        byte[] buffer2 = this.UrlEncodeNonAscii(bytes, 0, bytes.Length, false);
        return Encoding.ASCII.GetString(buffer2);
    }

    internal byte[] UrlEncodeNonAscii(byte[] bytes, int offset, int count, bool alwaysCreateNewReturnValue)
    {
        if (!ValidateUrlEncodingParameters(bytes, offset, count))
        {
            return null;
        }
        int num = 0;
        for (int i = 0; i < count; i++)
        {
            if (IsNonAsciiByte(bytes[offset + i]))
            {
                num++;
            }
        }
        if (!alwaysCreateNewReturnValue && (num == 0))
        {
            return bytes;
        }
        byte[] buffer = new byte[count + (num * 2)];
        int num3 = 0;
        for (int j = 0; j < count; j++)
        {
            byte b = bytes[offset + j];
            if (IsNonAsciiByte(b))
            {
                buffer[num3++] = 0x25;
                buffer[num3++] = (byte) HttpEncoderUtility.IntToHex((b >> 4) & 15);
                buffer[num3++] = (byte) HttpEncoderUtility.IntToHex(b & 15);
            }
            else
            {
                buffer[num3++] = b;
            }
        }
        return buffer;
    }

    internal string UrlEncodeUnicode(string value, bool ignoreAscii)
    {
        if (value == null)
        {
            return null;
        }
        int length = value.Length;
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++)
        {
            char ch = value[i];
            if ((ch & 0xff80) == 0)
            {
                if (ignoreAscii || HttpEncoderUtility.IsUrlSafeChar(ch))
                {
                    builder.Append(ch);
                }
                else if (ch == ' ')
                {
                    builder.Append('+');
                }
                else
                {
                    builder.Append('%');
                    builder.Append(HttpEncoderUtility.IntToHex((ch >> 4) & '\x000f'));
                    builder.Append(HttpEncoderUtility.IntToHex(ch & '\x000f'));
                }
            }
            else
            {
                builder.Append("%u");
                builder.Append(HttpEncoderUtility.IntToHex((ch >> 12) & '\x000f'));
                builder.Append(HttpEncoderUtility.IntToHex((ch >> 8) & '\x000f'));
                builder.Append(HttpEncoderUtility.IntToHex((ch >> 4) & '\x000f'));
                builder.Append(HttpEncoderUtility.IntToHex(ch & '\x000f'));
            }
        }
        return builder.ToString();
    }

    protected internal virtual string UrlPathEncode(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return value;
        }
        int index = value.IndexOf('?');
        if (index >= 0)
        {
            return (this.UrlPathEncode(value.Substring(0, index)) + value.Substring(index));
        }
        return HttpEncoderUtility.UrlEncodeSpaces(this.UrlEncodeNonAscii(value, Encoding.UTF8));
    }

    internal byte[] UrlTokenDecode(string input)
    {
        if (input == null)
        {
            throw new ArgumentNullException("input");
        }
        int length = input.Length;
        if (length < 1)
        {
            return new byte[0];
        }
        int num2 = input[length - 1] - '0';
        if ((num2 < 0) || (num2 > 10))
        {
            return null;
        }
        char[] inArray = new char[(length - 1) + num2];
        for (int i = 0; i < (length - 1); i++)
        {
            char ch = input[i];
            switch (ch)
            {
                case '-':
                    inArray[i] = '+';
                    break;

                case '_':
                    inArray[i] = '/';
                    break;

                default:
                    inArray[i] = ch;
                    break;
            }
        }
        for (int j = length - 1; j < inArray.Length; j++)
        {
            inArray[j] = '=';
        }
        return Convert.FromBase64CharArray(inArray, 0, inArray.Length);
    }

    internal string UrlTokenEncode(byte[] input)
    {
        if (input == null)
        {
            throw new ArgumentNullException("input");
        }
        if (input.Length < 1)
        {
            return string.Empty;
        }
        string str = null;
        int index = 0;
        char[] chArray = null;
        str = Convert.ToBase64String(input);
        if (str == null)
        {
            return null;
        }
        index = str.Length;
        while (index > 0)
        {
            if (str[index - 1] != '=')
            {
                break;
            }
            index--;
        }
        chArray = new char[index + 1];
        chArray[index] = (char) ((0x30 + str.Length) - index);
        for (int i = 0; i < index; i++)
        {
            char ch = str[i];
            switch (ch)
            {
                case '+':
                    chArray[i] = '-';
                    break;

                case '/':
                    chArray[i] = '_';
                    break;

                case '=':
                    chArray[i] = ch;
                    break;

                default:
                    chArray[i] = ch;
                    break;
            }
        }
        return new string(chArray);
    }

    private static bool ValidateUrlEncodingParameters(byte[] bytes, int offset, int count)
    {
        if ((bytes == null) && (count == 0))
        {
            return false;
        }
        if (bytes == null)
        {
            throw new ArgumentNullException("bytes");
        }
        if ((offset < 0) || (offset > bytes.Length))
        {
            throw new ArgumentOutOfRangeException("offset");
        }
        if ((count < 0) || ((offset + count) > bytes.Length))
        {
            throw new ArgumentOutOfRangeException("count");
        }
        return true;
    }

    // Properties
    public static HttpEncoder Current
    {
        get
        {
            HttpContext current = HttpContext.Current;
            if (((current != null) && (current.Response != null)) && current.Response.DisableCustomHttpEncoder)
            {
                return _defaultEncoder;
            }
            if (_customEncoder == null)
            {
                _customEncoder = _customEncoderResolver.Value;
            }
            return _customEncoder;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            _customEncoder = value;
            _isDefaultEncoder = value.GetType() == typeof(HttpEncoder);
        }
    }

    public static HttpEncoder Default
    {
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        get
        {
            return _defaultEncoder;
        }
    }

    // Nested Types
    private class UrlDecoder
    {
        // Fields
        private int _bufferSize;
        private byte[] _byteBuffer;
        private char[] _charBuffer;
        private Encoding _encoding;
        private int _numBytes;
        private int _numChars;

        // Methods
        internal UrlDecoder(int bufferSize, Encoding encoding)
        {
            this._bufferSize = bufferSize;
            this._encoding = encoding;
            this._charBuffer = new char[bufferSize];
        }

        internal void AddByte(byte b)
        {
            if (this._byteBuffer == null)
            {
                this._byteBuffer = new byte[this._bufferSize];
            }
            this._byteBuffer[this._numBytes++] = b;
        }

        internal void AddChar(char ch)
        {
            if (this._numBytes > 0)
            {
                this.FlushBytes();
            }
            this._charBuffer[this._numChars++] = ch;
        }

        private void FlushBytes()
        {
            if (this._numBytes > 0)
            {
                this._numChars += this._encoding.GetChars(this._byteBuffer, 0, this._numBytes, this._charBuffer, this._numChars);
                this._numBytes = 0;
            }
        }

        internal string GetString()
        {
            if (this._numBytes > 0)
            {
                this.FlushBytes();
            }
            if (this._numChars > 0)
            {
                return new string(this._charBuffer, 0, this._numChars);
            }
            return string.Empty;
        }
    }
}
    internal static class HttpEncoderUtility
    {
        // Methods
        public static int HexToInt(char h)
        {
            if ((h >= '0') && (h <= '9'))
            {
                return (h - '0');
            }
            if ((h >= 'a') && (h <= 'f'))
            {
                return ((h - 'a') + 10);
            }
            if ((h >= 'A') && (h <= 'F'))
            {
                return ((h - 'A') + 10);
            }
            return -1;
        }

        public static char IntToHex(int n)
        {
            if (n <= 9)
            {
                return (char)(n + 0x30);
            }
            return (char)((n - 10) + 0x61);
        }

        public static bool IsUrlSafeChar(char ch)
        {
            if ((((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'))) || ((ch >= '0') && (ch <= '9')))
            {
                return true;
            }
            switch (ch)
            {
                case '(':
                case ')':
                case '*':
                case '-':
                case '.':
                case '_':
                case '!':
                    return true;
            }
            return false;
        }

        internal static string UrlEncodeSpaces(string str)
        {
            if ((str != null) && (str.IndexOf(' ') >= 0))
            {
                str = str.Replace(" ", "%20");
            }
            return str;
        }
    }
    public sealed class HttpWriter : TextWriter
    {
        // Fields
        private ArrayList _buffers;
        private char[] _charBuffer;
        private int _charBufferFree;
        private int _charBufferLength;
        private HttpResponseStreamFilterSink _filterSink;
        private bool _hasBeenClearedRecently;
        private bool _ignoringFurtherWrites;
        private Stream _installedFilter;
        private HttpBaseMemoryResponseBufferElement _lastBuffer;
        private HttpResponse _response;
        private bool _responseBufferingOn;
        private int _responseCodePage;
        private bool _responseCodePageIsAsciiCompat;
        private Encoder _responseEncoder;
        private Encoding _responseEncoding;
        private bool _responseEncodingUpdated;
        private bool _responseEncodingUsed;
        private HttpResponseStream _stream;
        private ArrayList _substElements;
        private static CharBufferAllocator s_Allocator = new CharBufferAllocator(0x400, 0x40);

        // Methods
        internal HttpWriter(HttpResponse response)
            : base(null)
        {
            this._response = response;
            this._stream = new HttpResponseStream(this);
            this._buffers = new ArrayList();
            this._lastBuffer = null;
            this._charBuffer = (char[])s_Allocator.GetBuffer();
            this._charBufferLength = this._charBuffer.Length;
            this._charBufferFree = this._charBufferLength;
            this.UpdateResponseBuffering();
        }

        private void BufferData(byte[] data, int offset, int size, bool needToCopyData)
        {
            int num;
            if (this._lastBuffer != null)
            {
                num = this._lastBuffer.Append(data, offset, size);
                size -= num;
                offset += num;
            }
            else if ((!needToCopyData && (offset == 0)) && !this._responseBufferingOn)
            {
                this._buffers.Add(new HttpResponseBufferElement(data, size));
                return;
            }
            while (size > 0)
            {
                this._lastBuffer = this.CreateNewMemoryBufferElement();
                this._buffers.Add(this._lastBuffer);
                num = this._lastBuffer.Append(data, offset, size);
                offset += num;
                size -= num;
            }
        }

        private void BufferResource(IntPtr data, int offset, int size)
        {
            if ((size > 0x1000) || !this._responseBufferingOn)
            {
                this._lastBuffer = null;
                this._buffers.Add(new HttpResourceResponseElement(data, offset, size));
            }
            else
            {
                int num;
                if (this._lastBuffer != null)
                {
                    num = this._lastBuffer.Append(data, offset, size);
                    size -= num;
                    offset += num;
                }
                while (size > 0)
                {
                    this._lastBuffer = this.CreateNewMemoryBufferElement();
                    this._buffers.Add(this._lastBuffer);
                    num = this._lastBuffer.Append(data, offset, size);
                    offset += num;
                    size -= num;
                }
            }
        }

        internal void ClearBuffers()
        {
            this.ClearCharBuffer();
            if (this._substElements != null)
            {
                this._response.Context.Request.SetDynamicCompression(true);
            }
            this.RecycleBufferElements();
            this._buffers = new ArrayList();
            this._lastBuffer = null;
            this._hasBeenClearedRecently = true;
        }

        private void ClearCharBuffer()
        {
            this._charBufferFree = this._charBufferLength;
        }

        internal void ClearSubstitutionBlocks()
        {
            this._substElements = null;
        }

        public override void Close()
        {
        }

        private HttpBaseMemoryResponseBufferElement CreateNewMemoryBufferElement()
        {
            return new HttpResponseUnmanagedBufferElement();
        }

        internal void DisposeIntegratedBuffers()
        {
            if (this._buffers != null)
            {
                int count = this._buffers.Count;
                for (int i = 0; i < count; i++)
                {
                    HttpBaseMemoryResponseBufferElement element = this._buffers[i] as HttpBaseMemoryResponseBufferElement;
                    if (element != null)
                    {
                        element.Recycle();
                    }
                }
                this._buffers = null;
            }
            this.ClearBuffers();
        }

        internal void Filter(bool finalFiltering)
        {
            if (this._installedFilter != null)
            {
                if (this._charBufferLength != this._charBufferFree)
                {
                    this.FlushCharBuffer(true);
                }
                this._lastBuffer = null;
                if ((this._buffers.Count != 0) || finalFiltering)
                {
                    ArrayList list = this._buffers;
                    this._buffers = new ArrayList();
                    this._filterSink.Filtering = true;
                    try
                    {
                        int count = list.Count;
                        for (int i = 0; i < count; i++)
                        {
                            IHttpResponseElement element = (IHttpResponseElement)list[i];
                            long size = element.GetSize();
                            if (size > 0L)
                            {
                                this._installedFilter.Write(element.GetBytes(), 0, Convert.ToInt32(size));
                            }
                        }
                        this._installedFilter.Flush();
                    }
                    finally
                    {
                        try
                        {
                            if (finalFiltering)
                            {
                                this._installedFilter.Close();
                            }
                        }
                        finally
                        {
                            this._filterSink.Filtering = false;
                        }
                    }
                }
            }
        }

        internal void FilterIntegrated(bool finalFiltering, IIS7WorkerRequest wr)
        {
            if (this._installedFilter != null)
            {
                if (this._charBufferLength != this._charBufferFree)
                {
                    this.FlushCharBuffer(true);
                }
                this._lastBuffer = null;
                ArrayList list = this._buffers;
                this._buffers = new ArrayList();
                ArrayList list2 = null;
                bool hasSubstBlocks = false;
                list2 = wr.GetBufferedResponseChunks(false, null, ref hasSubstBlocks);
                this._filterSink.Filtering = true;
                try
                {
                    if (list2 != null)
                    {
                        for (int i = 0; i < list2.Count; i++)
                        {
                            IHttpResponseElement element = (IHttpResponseElement)list2[i];
                            long size = element.GetSize();
                            if (size > 0L)
                            {
                                this._installedFilter.Write(element.GetBytes(), 0, Convert.ToInt32(size));
                            }
                        }
                        wr.ClearResponse(true, false);
                    }
                    if (list != null)
                    {
                        for (int j = 0; j < list.Count; j++)
                        {
                            IHttpResponseElement element2 = (IHttpResponseElement)list[j];
                            long num4 = element2.GetSize();
                            if (num4 > 0L)
                            {
                                this._installedFilter.Write(element2.GetBytes(), 0, Convert.ToInt32(num4));
                            }
                        }
                    }
                    this._installedFilter.Flush();
                }
                finally
                {
                    try
                    {
                        if (finalFiltering)
                        {
                            this._installedFilter.Close();
                        }
                    }
                    finally
                    {
                        this._filterSink.Filtering = false;
                    }
                }
            }
        }

        public override void Flush()
        {
        }

        private void FlushCharBuffer(bool flushEncoder)
        {
            int charCount = this._charBufferLength - this._charBufferFree;
            if (!this._responseEncodingUpdated)
            {
                this.UpdateResponseEncoding();
            }
            this._responseEncodingUsed = true;
            int maxByteCount = this._responseEncoding.GetMaxByteCount(charCount);
            if ((maxByteCount <= 0x80) || !this._responseBufferingOn)
            {
                byte[] bytes = new byte[maxByteCount];
                int size = this._responseEncoder.GetBytes(this._charBuffer, 0, charCount, bytes, 0, flushEncoder);
                this.BufferData(bytes, 0, size, false);
            }
            else
            {
                int freeBytes = (this._lastBuffer != null) ? this._lastBuffer.FreeBytes : 0;
                if (freeBytes < maxByteCount)
                {
                    this._lastBuffer = this.CreateNewMemoryBufferElement();
                    this._buffers.Add(this._lastBuffer);
                    freeBytes = this._lastBuffer.FreeBytes;
                }
                this._lastBuffer.AppendEncodedChars(this._charBuffer, 0, charCount, this._responseEncoder, flushEncoder);
            }
            this._charBufferFree = this._charBufferLength;
        }

        internal long GetBufferedLength()
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            long num = 0L;
            if (this._buffers != null)
            {
                int count = this._buffers.Count;
                for (int i = 0; i < count; i++)
                {
                    num += ((IHttpResponseElement)this._buffers[i]).GetSize();
                }
            }
            return num;
        }

        internal Stream GetCurrentFilter()
        {
            if (this._installedFilter != null)
            {
                return this._installedFilter;
            }
            if (this._filterSink == null)
            {
                this._filterSink = new HttpResponseStreamFilterSink(this);
            }
            return this._filterSink;
        }

        internal ArrayList GetIntegratedSnapshot(out bool hasSubstBlocks, IIS7WorkerRequest wr)
        {
            ArrayList list = null;
            ArrayList snapshot = this.GetSnapshot(out hasSubstBlocks);
            ArrayList list3 = wr.GetBufferedResponseChunks(true, this._substElements, ref hasSubstBlocks);
            if (list3 != null)
            {
                for (int i = 0; i < snapshot.Count; i++)
                {
                    list3.Add(snapshot[i]);
                }
                list = list3;
            }
            else
            {
                list = snapshot;
            }
            if ((this._substElements != null) && (this._substElements.Count > 0))
            {
                int num2 = 0;
                for (int j = 0; j < list.Count; j++)
                {
                    if (list[j] is HttpSubstBlockResponseElement)
                    {
                        num2++;
                        if (num2 == this._substElements.Count)
                        {
                            break;
                        }
                    }
                }
                if (num2 != this._substElements.Count)
                {
                    throw new InvalidOperationException(SR.GetString("Substitution_blocks_cannot_be_modified"));
                }
                this._response.Context.Request.SetDynamicCompression(true);
            }
            return list;
        }

        internal int GetResponseBufferCountAfterFlush()
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this._lastBuffer = null;
            return this._buffers.Count;
        }

        internal ArrayList GetSnapshot(out bool hasSubstBlocks)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this._lastBuffer = null;
            hasSubstBlocks = false;
            ArrayList list = new ArrayList();
            int count = this._buffers.Count;
            for (int i = 0; i < count; i++)
            {
                object obj2 = this._buffers[i];
                HttpBaseMemoryResponseBufferElement element = obj2 as HttpBaseMemoryResponseBufferElement;
                if (element != null)
                {
                    if (element.FreeBytes > 0x1000)
                    {
                        obj2 = element.Clone();
                    }
                    else
                    {
                        element.DisableRecycling();
                    }
                }
                else if (obj2 is HttpSubstBlockResponseElement)
                {
                    hasSubstBlocks = true;
                }
                list.Add(obj2);
            }
            return list;
        }

        internal void IgnoreFurtherWrites()
        {
            this._ignoringFurtherWrites = true;
        }

        internal void InstallFilter(Stream filter)
        {
            if (this._filterSink == null)
            {
                throw new HttpException(SR.GetString("Invalid_response_filter"));
            }
            this._installedFilter = filter;
        }

        internal void MoveResponseBufferRangeForward(int srcIndex, int srcCount, int dstIndex)
        {
            if (srcCount > 0)
            {
                object[] array = new object[srcIndex - dstIndex];
                this._buffers.CopyTo(dstIndex, array, 0, array.Length);
                for (int i = 0; i < srcCount; i++)
                {
                    this._buffers[dstIndex + i] = this._buffers[srcIndex + i];
                }
                for (int j = 0; j < array.Length; j++)
                {
                    this._buffers[(dstIndex + srcCount) + j] = array[j];
                }
            }
            HttpBaseMemoryResponseBufferElement element = this._buffers[this._buffers.Count - 1] as HttpBaseMemoryResponseBufferElement;
            if ((element != null) && (element.FreeBytes > 0))
            {
                this._lastBuffer = element;
            }
            else
            {
                this._lastBuffer = null;
            }
        }

        private void RecycleBufferElements()
        {
            if (this._buffers != null)
            {
                int count = this._buffers.Count;
                for (int i = 0; i < count; i++)
                {
                    HttpBaseMemoryResponseBufferElement element = this._buffers[i] as HttpBaseMemoryResponseBufferElement;
                    if (element != null)
                    {
                        element.Recycle();
                    }
                }
                this._buffers = null;
            }
        }

        internal void RecycleBuffers()
        {
            if (this._charBuffer != null)
            {
                s_Allocator.ReuseBuffer(this._charBuffer);
                this._charBuffer = null;
            }
            this.RecycleBufferElements();
        }

        internal void Send(HttpWorkerRequest wr)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            int count = this._buffers.Count;
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    ((IHttpResponseElement)this._buffers[i]).Send(wr);
                }
            }
        }

        internal void TransmitFile(string filename, long offset, long size, bool isImpersonating, bool supportsLongTransmitFile)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this._lastBuffer = null;
            this._buffers.Add(new HttpFileResponseElement(filename, offset, size, isImpersonating, supportsLongTransmitFile));
            if (!this._responseBufferingOn)
            {
                this._response.Flush();
            }
        }

        internal void UpdateResponseBuffering()
        {
            this._responseBufferingOn = this._response.BufferOutput;
        }

        internal void UpdateResponseEncoding()
        {
            if (this._responseEncodingUpdated && (this._charBufferLength != this._charBufferFree))
            {
                this.FlushCharBuffer(true);
            }
            this._responseEncoding = this._response.ContentEncoding;
            this._responseEncoder = this._response.ContentEncoder;
            this._responseCodePage = this._responseEncoding.CodePage;
            this._responseCodePageIsAsciiCompat = CodePageUtils.IsAsciiCompatibleCodePage(this._responseCodePage);
            this._responseEncodingUpdated = true;
        }

        internal void UseSnapshot(ArrayList buffers)
        {
            this.ClearBuffers();
            int count = buffers.Count;
            for (int i = 0; i < count; i++)
            {
                object obj2 = buffers[i];
                HttpSubstBlockResponseElement element = obj2 as HttpSubstBlockResponseElement;
                if (element != null)
                {
                    this._buffers.Add(element.Substitute(this.Encoding));
                }
                else
                {
                    this._buffers.Add(obj2);
                }
            }
        }

        public override void Write(char ch)
        {
            if (!this._ignoringFurtherWrites)
            {
                if (this._charBufferFree == 0)
                {
                    this.FlushCharBuffer(false);
                }
                this._charBuffer[this._charBufferLength - this._charBufferFree] = ch;
                this._charBufferFree--;
                if (!this._responseBufferingOn)
                {
                    this._response.Flush();
                }
            }
        }

        public override void Write(object obj)
        {
            if (!this._ignoringFurtherWrites && (obj != null))
            {
                this.Write(obj.ToString());
            }
        }

        public override void Write(string s)
        {
            if (!this._ignoringFurtherWrites && (s != null))
            {
                if (s.Length != 0)
                {
                    if (s.Length < this._charBufferFree)
                    {
                        StringUtil.UnsafeStringCopy(s, 0, this._charBuffer, this._charBufferLength - this._charBufferFree, s.Length);
                        this._charBufferFree -= s.Length;
                    }
                    else
                    {
                        int length = s.Length;
                        int srcIndex = 0;
                        while (length > 0)
                        {
                            if (this._charBufferFree == 0)
                            {
                                this.FlushCharBuffer(false);
                            }
                            int len = (length < this._charBufferFree) ? length : this._charBufferFree;
                            StringUtil.UnsafeStringCopy(s, srcIndex, this._charBuffer, this._charBufferLength - this._charBufferFree, len);
                            this._charBufferFree -= len;
                            srcIndex += len;
                            length -= len;
                        }
                    }
                }
                if (!this._responseBufferingOn)
                {
                    this._response.Flush();
                }
            }
        }

        public override void Write(char[] buffer, int index, int count)
        {
            if (!this._ignoringFurtherWrites)
            {
                if (buffer == null)
                {
                    throw new ArgumentNullException("buffer");
                }
                if (index < 0)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                if (count < 0)
                {
                    throw new ArgumentOutOfRangeException("count");
                }
                if ((buffer.Length - index) < count)
                {
                    throw new ArgumentException(SR.GetString("InvalidOffsetOrCount", new object[] { "index", "count" }));
                }
                if (count != 0)
                {
                    while (count > 0)
                    {
                        if (this._charBufferFree == 0)
                        {
                            this.FlushCharBuffer(false);
                        }
                        int length = (count < this._charBufferFree) ? count : this._charBufferFree;
                        Array.Copy(buffer, index, this._charBuffer, this._charBufferLength - this._charBufferFree, length);
                        this._charBufferFree -= length;
                        index += length;
                        count -= length;
                    }
                    if (!this._responseBufferingOn)
                    {
                        this._response.Flush();
                    }
                }
            }
        }

        public void WriteBytes(byte[] buffer, int index, int count)
        {
            if (!this._ignoringFurtherWrites)
            {
                this.WriteFromStream(buffer, index, count);
            }
        }

        internal void WriteFile(string filename, long offset, long size)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this._lastBuffer = null;
            this._buffers.Add(new HttpFileResponseElement(filename, offset, size));
            if (!this._responseBufferingOn)
            {
                this._response.Flush();
            }
        }

        internal void WriteFromStream(byte[] data, int offset, int size)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this.BufferData(data, offset, size, true);
            if (!this._responseBufferingOn)
            {
                this._response.Flush();
            }
        }

        public override void WriteLine()
        {
            if (!this._ignoringFurtherWrites)
            {
                if (this._charBufferFree < 2)
                {
                    this.FlushCharBuffer(false);
                }
                int index = this._charBufferLength - this._charBufferFree;
                this._charBuffer[index] = '\r';
                this._charBuffer[index + 1] = '\n';
                this._charBufferFree -= 2;
                if (!this._responseBufferingOn)
                {
                    this._response.Flush();
                }
            }
        }

        public void WriteString(string s, int index, int count)
        {
            if (s != null)
            {
                if (index < 0)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                if (count < 0)
                {
                    throw new ArgumentOutOfRangeException("count");
                }
                if ((index + count) > s.Length)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                if (!this._ignoringFurtherWrites)
                {
                    if (count != 0)
                    {
                        if (count >= this._charBufferFree)
                        {
                            while (count > 0)
                            {
                                if (this._charBufferFree == 0)
                                {
                                    this.FlushCharBuffer(false);
                                }
                                int len = (count < this._charBufferFree) ? count : this._charBufferFree;
                                StringUtil.UnsafeStringCopy(s, index, this._charBuffer, this._charBufferLength - this._charBufferFree, len);
                                this._charBufferFree -= len;
                                index += len;
                                count -= len;
                            }
                        }
                        else
                        {
                            StringUtil.UnsafeStringCopy(s, index, this._charBuffer, this._charBufferLength - this._charBufferFree, count);
                            this._charBufferFree -= count;
                        }
                    }
                    if (!this._responseBufferingOn)
                    {
                        this._response.Flush();
                    }
                }
            }
        }

        internal void WriteSubstBlock(HttpResponseSubstitutionCallback callback, IIS7WorkerRequest iis7WorkerRequest)
        {
            if (this._charBufferLength != this._charBufferFree)
            {
                this.FlushCharBuffer(true);
            }
            this._lastBuffer = null;
            IHttpResponseElement element = new HttpSubstBlockResponseElement(callback, this.Encoding, this.Encoder, iis7WorkerRequest);
            this._buffers.Add(element);
            if (iis7WorkerRequest != null)
            {
                this.SubstElements.Add(element);
            }
            if (!this._responseBufferingOn)
            {
                this._response.Flush();
            }
        }

        internal void WriteUTF8ResourceString(IntPtr pv, int offset, int size, bool asciiOnly)
        {
            if (!this._responseEncodingUpdated)
            {
                this.UpdateResponseEncoding();
            }
            if ((this._responseCodePage == 0xfde9) || (asciiOnly && this._responseCodePageIsAsciiCompat))
            {
                this._responseEncodingUsed = true;
                if (this._charBufferLength != this._charBufferFree)
                {
                    this.FlushCharBuffer(true);
                }
                this.BufferResource(pv, offset, size);
                if (!this._responseBufferingOn)
                {
                    this._response.Flush();
                }
            }
            else
            {
                this.Write(StringResourceManager.ResourceToString(pv, offset, size));
            }
        }

        // Properties
        internal Encoder Encoder
        {
            get
            {
                if (!this._responseEncodingUpdated)
                {
                    this.UpdateResponseEncoding();
                }
                return this._responseEncoder;
            }
        }

        public override Encoding Encoding
        {
            get
            {
                if (!this._responseEncodingUpdated)
                {
                    this.UpdateResponseEncoding();
                }
                return this._responseEncoding;
            }
        }

        internal bool FilterInstalled
        {
            get
            {
                return (this._installedFilter != null);
            }
        }

        internal bool HasBeenClearedRecently
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            get
            {
                return this._hasBeenClearedRecently;
            }
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            set
            {
                this._hasBeenClearedRecently = value;
            }
        }

        internal bool IgnoringFurtherWrites
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            get
            {
                return this._ignoringFurtherWrites;
            }
        }

        public Stream OutputStream
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            get
            {
                return this._stream;
            }
        }

        internal bool ResponseEncodingUsed
        {
            [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
            get
            {
                return this._responseEncodingUsed;
            }
        }

        internal ArrayList SubstElements
        {
            get
            {
                if (this._substElements == null)
                {
                    this._substElements = new ArrayList();
                    this._response.Context.Request.SetDynamicCompression(false);
                }
                return this._substElements;
            }
        }
    }

}
