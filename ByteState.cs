﻿using System;

namespace HSNetwork
{
    /// <summary>
    /// 바이트 현재 상태입니다.
    /// </summary>                      
    public enum NetworkMeasure { Byte, KByte, MByte, GByte, TByte, PByte, OutByte }
    public class ByteState
    {
        /// <summary>
        /// 바이트 상태를 나눠줍니다.
        /// </summary>
        /// <param name="Byte">네트워크 바이트 입니다.</param>
        /// <param name="Dec">true면 1000으로 계산하고 false면 1024으로 계산합니다.</param>
        /// <param name="Measure">바이트 현재 상태를 반환합니다.</param>
        /// <returns<</returns<
        public static double MeasureCaculate(long Byte, bool Dec, out NetworkMeasure Measure)
        {
            if (Dec)
            {
                if (Byte < 1000) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1000000) { Measure = NetworkMeasure.KByte; return Byte / 1000; }
                else if (Byte < 1000000000) { Measure = NetworkMeasure.MByte; return Byte / 1000000; }
                else if (Byte < 1000000000000) { Measure = NetworkMeasure.GByte; return (Byte / 1000000000); }
                else if (Byte < 1000000000000000) { Measure = NetworkMeasure.TByte; return Byte / 1000000000000; }
                else if (Byte < 1000000000000000000) { Measure = NetworkMeasure.PByte; return Byte / 1000000000000000; }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
            else
            {
                if (Byte < 1024) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1048576) { Measure = NetworkMeasure.KByte; return Byte / 1024; }
                else if (Byte < 1073741824) { Measure = NetworkMeasure.MByte; return Byte / 1048576; }
                else if (Byte < 1099511627776) { Measure = NetworkMeasure.GByte; return Byte / 1073741824; }
                else if (Byte < 1125899906842624) { Measure = NetworkMeasure.TByte; return Byte / 1099511627776; }
                else if (Byte < 1152921504606846976) { Measure = NetworkMeasure.PByte; return Byte / 1125899906842624; }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
        }
        /// 바이트 상태를 나눠줍니다.
        /// </summary>
        /// <param name="Byte">네트워크 바이트 입니다.</param>
        /// <param name="Dec">true면 1000으로 계산하고 false면 1024으로 계산합니다.</param>
        /// <param name="최대자릿수">지정한 자릿수까지만 표시됩니다.</param>
        /// <param name="반올림">자릿수를 올릴때 반올림을 할지 결정합니다.</param>
        /// <param name="Measure">바이트 현재 상태를 반환합니다.</param>
        /// <returns<</returns<
        public static double MeasureCaculate(long Byte, bool Dec, byte 최대자릿수, bool 반올림, out NetworkMeasure Measure)
        {
            if (Dec)
            {
                if (Byte < 1000) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1000000) { Measure = NetworkMeasure.KByte; return Convert.ToDouble((Byte / 1000).ToString("N" + 최대자릿수)); }
                else if (Byte < 1000000000) { Measure = NetworkMeasure.MByte; return Convert.ToDouble((Byte / 1000000).ToString("N" + 최대자릿수)); }
                else if (Byte < 1000000000000) { Measure = NetworkMeasure.GByte; return Convert.ToDouble((Byte / 1000000000).ToString("N" + 최대자릿수)); }
                else if (Byte < 1000000000000000) { Measure = NetworkMeasure.TByte; return Convert.ToDouble((Byte / 1000000000000).ToString("N" + 최대자릿수)); }
                else if (Byte < 1000000000000000000) { Measure = NetworkMeasure.PByte; return Convert.ToDouble((Byte / 1000000000000000).ToString("N" + 최대자릿수)); }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
            else
            {
                if (Byte < 1024) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1048576) { Measure = NetworkMeasure.KByte; return Convert.ToDouble((Byte / 1024).ToString("N" + 최대자릿수)); ; }
                else if (Byte < 1073741824) { Measure = NetworkMeasure.MByte; return Convert.ToDouble((Byte / 1048576).ToString("N" + 최대자릿수)); ; }
                else if (Byte < 1099511627776) { Measure = NetworkMeasure.GByte; return Convert.ToDouble((Byte / 1073741824).ToString("N" + 최대자릿수)); ; }
                else if (Byte < 1125899906842624) { Measure = NetworkMeasure.TByte; return Convert.ToDouble((Byte / 1099511627776).ToString("N" + 최대자릿수)); ; }
                else if (Byte < 1152921504606846976) { Measure = NetworkMeasure.PByte; return Convert.ToDouble((Byte / 1125899906842624).ToString("N" + 최대자릿수)); ; }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
        }

        /// <summary<
        /// 바이트 상태를 나눠줍니다.
        /// </summary>
        /// <param name="Byte">네트워크 바이트 입니다.</param>
        /// <param name="Dec">true면 1000으로 계산하고 false면 1024으로 계산합니다.</param>
        /// <param name="Measure">바이트 현재 상태를 반환합니다.</param>
        /// <returns<</returns<
        public static double MeasureCaculate(double Byte, bool Dec, out NetworkMeasure Measure)
        {
            if (Dec)
            {
                if (Byte < 1000) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1000000) { Measure = NetworkMeasure.KByte; return Byte / 1000; }
                else if (Byte < 1000000000) { Measure = NetworkMeasure.MByte; return Byte / 1000000; }
                else if (Byte < 1000000000000) { Measure = NetworkMeasure.GByte; return (Byte / 1000000000); }
                else if (Byte < 1000000000000000) { Measure = NetworkMeasure.TByte; return Byte / 1000000000000; }
                else if (Byte < 1000000000000000000) { Measure = NetworkMeasure.PByte; return Byte / 1000000000000000; }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
            else
            {
                if (Byte < 1024) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1048576) { Measure = NetworkMeasure.KByte; return Byte / 1024; }
                else if (Byte < 1073741824) { Measure = NetworkMeasure.MByte; return Byte / 1048576; }
                else if (Byte < 1099511627776) { Measure = NetworkMeasure.GByte; return Byte / 1073741824; }
                else if (Byte < 1125899906842624) { Measure = NetworkMeasure.TByte; return Byte / 1099511627776; }
                else if (Byte < 1152921504606846976) { Measure = NetworkMeasure.PByte; return Byte / 1125899906842624; }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
        }

        /// <summary<
        /// 바이트 상태를 나눠줍니다.
        /// </summary>
        /// <param name="Byte">네트워크 바이트 입니다.</param>
        /// <param name="Dec">true면 1000으로 계산하고 false면 1024으로 계산합니다.</param>
        /// <param name="최대자릿수">지정한 자릿수까지만 표시됩니다.</param>
        /// <param name="Round">자릿수를 올릴떄 반올림을 할지 결정합니다.</param>
        /// <param name="Measure">바이트 현재 상태를 반환합니다.</param>
        /// <returns<</returns<
        public static double MeasureCaculate(double Byte, bool Dec, byte 최대자릿수/*, bool Round*/, out NetworkMeasure Measure)
        {
            if (Dec)
            {
                if (Byte < 1000) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1000000) { Measure = NetworkMeasure.KByte; return Math.Round(Byte / 1000, 최대자릿수); }
                else if (Byte < 1000000000) { Measure = NetworkMeasure.MByte; return Math.Round(Byte / 1000000, 최대자릿수); }
                else if (Byte < 1000000000000) { Measure = NetworkMeasure.GByte; return Math.Round(Byte / 1000000000, 최대자릿수); }
                else if (Byte < 1000000000000000) { Measure = NetworkMeasure.TByte; return Math.Round(Byte / 1000000000000, 최대자릿수); ; }
                else if (Byte < 1000000000000000000) { Measure = NetworkMeasure.PByte; return Math.Round(Byte / 1000000000000000, 최대자릿수); }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
            else
            {
                if (Byte < 1024) { Measure = NetworkMeasure.Byte; return (double)Byte; }
                else if (Byte < 1048576) { Measure = NetworkMeasure.KByte; return Math.Round(Byte / 1024, 최대자릿수); }
                else if (Byte < 1073741824) { Measure = NetworkMeasure.MByte; return Math.Round(Byte / 1048576, 최대자릿수); }
                else if (Byte < 1099511627776) { Measure = NetworkMeasure.GByte; return Math.Round(Byte / 1073741824, 최대자릿수); }
                else if (Byte < 1125899906842624) { Measure = NetworkMeasure.TByte; return Math.Round(Byte / 1099511627776, 최대자릿수); }
                else if (Byte < 1152921504606846976) { Measure = NetworkMeasure.PByte; return Math.Round(Byte / 1125899906842624, 최대자릿수); }
                else { Measure = NetworkMeasure.OutByte; return -1; }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Byte">바이트 크기 입니다</param>
        /// <param name="Dec">true면 1000으로 계산하고 false면 1024으로 계산합니다.</param>
        /// <param name="Space">숫자와 문자 중간에 띄어쓰기를 할지 여부입니다.</param>
        /// <param name="DisplayFullMeasure">True면 'Byte' 를 표시하고 False 면 'B' 만 표시합니다.</param>
        /// <param name="최대자릿수">지정한 자릿수까지만 표시됩니다.</param>
        /// <returns></returns>
        public static string NetworkString(long Byte, bool Dec, bool Space,bool DisplayFullMeasure = false, byte 최대자릿수 = 2)
        {
            NetworkMeasure nm = NetworkMeasure.OutByte;
            double a = MeasureCaculate(Byte, Dec, 최대자릿수, out nm);

            return string.Format(Space?"{0} {1}":"{0}{1}", a.ToString("0.00") ,NetworkMeasureString(nm, DisplayFullMeasure));
        }
        public static string NetworkMeasureString(NetworkMeasure Measure, bool DisplayFullMeasure)
        {
            NetworkMeasure nm = Measure;
            switch (nm)
            {
                case NetworkMeasure.Byte: return DisplayFullMeasure ? "Byte" : "B";
                case NetworkMeasure.KByte: return DisplayFullMeasure ? "KByte" : "KB";
                case NetworkMeasure.MByte: return DisplayFullMeasure ? "MByte" : "MB";
                case NetworkMeasure.GByte: return DisplayFullMeasure ? "GByte" : "GB";
                case NetworkMeasure.TByte: return DisplayFullMeasure ? "TByte" : "TB";
                case NetworkMeasure.PByte: return DisplayFullMeasure ? "PByte" : "PB";
                default: return DisplayFullMeasure ? "oByte" : "oB";
            }
        }
    }
}